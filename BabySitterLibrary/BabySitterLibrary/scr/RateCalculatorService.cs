﻿using System;

namespace BabySitterLibrary.scr
{
    public class RateCalculatorService
    {
        private ITimeValidationService _timeValidationServer;

        private const int Midnight = 24;
        private const int BeforeBedTimeRate = 12;
        private const int BeforeMidNightRate = 8;

        public RateCalculatorService(ITimeValidationService timeValidationService)
        {
            _timeValidationServer = timeValidationService;
        }

        public double CalculateRate(DateTime startTime, DateTime bedTime, DateTime endTime)
        {
            if (!_timeValidationServer.ValidateTime(startTime, bedTime, endTime))
            {
                return -1;
            }

            var roundedBedTime = RoundBedTime(bedTime);

            var bedTimeEarnings = (roundedBedTime.Hour - startTime.Hour) * BeforeBedTimeRate;
            var midNightEarings = (Midnight - roundedBedTime.Hour) * BeforeMidNightRate;

            return bedTimeEarnings + midNightEarings;
        }

        private static DateTime RoundBedTime(DateTime bedTime)
        {
            if (bedTime.Minute > 0 || bedTime.Second > 0)
            {
                return bedTime.AddHours(1);
            }
            return bedTime;
        }
    }
}