﻿using System;
using Moq;
using NUnit.Framework;

namespace BabySitterLibrary.scr
{
    [TestFixture]
    public class RateCalculatorServiceTest
    {
        private Mock<ITimeValidationService> _timeValidationServiceMock;
        private RateCalculatorService _rateCalculatorService;
        private readonly DateTime _endTime = new DateTime();


        [SetUp]
        public void SetUp()
        {
            _timeValidationServiceMock = new Mock<ITimeValidationService>();
            _rateCalculatorService = new RateCalculatorService(_timeValidationServiceMock.Object);

        }

        [Test]
        public void Works4HoursUntilBedTimeAnd3HoursUntilMidnightForValidStartAndBedTime()
        {
            var startTime = new DateTime(2015, 1, 1, 17, 0, 0);
            var bedTime = new DateTime(2015, 1, 2, 21, 0, 0);

            _timeValidationServiceMock.Setup(x => x.ValidateTime(startTime, bedTime, _endTime)).Returns(false);
            var rate = _rateCalculatorService.CalculateRate(startTime, bedTime, _endTime);

            //If invalid return -1 for now
            Assert.AreEqual(-1, rate);
            _timeValidationServiceMock.Verify(x => x.ValidateTime(startTime, bedTime, _endTime), Times.Once);
        }

        [Test]
        public void Works4HoursUntilBedTimeAnd3HoursUntilMidnight()
        {
            var startTime = new DateTime(2015, 1, 1, 17, 0, 0);
            var bedTime = new DateTime(2015, 1, 1, 21, 0, 0);
            _timeValidationServiceMock.Setup(x => x.ValidateTime(startTime, bedTime, _endTime)).Returns(true);

            var rate = _rateCalculatorService.CalculateRate(startTime, bedTime, _endTime);
            
            //4 hours for until bedtime = 12 * 4 = 48
            //3 hours until midnight = 3 * 8 = 24
            Assert.AreEqual(72, rate);
            _timeValidationServiceMock.Verify(x => x.ValidateTime(startTime, bedTime, _endTime), Times.Once);

        }

        [Test]
        public void Works3RoundedHoursHoursUntilBedTimeAnd4HoursUntilMidnight()
        {
            var startTime = new DateTime(2015, 1, 1, 17, 0, 0);
            var bedTime = new DateTime(2015, 1, 1, 19, 15, 12);
            _timeValidationServiceMock.Setup(x => x.ValidateTime(startTime, bedTime, _endTime)).Returns(true);

            var rate = _rateCalculatorService.CalculateRate(startTime, bedTime, _endTime);

            //3 hours for until bedtime = 3 * 12 = 36
            //4 hours until midnight = 4 * 8 = 32
            Assert.AreEqual(68, rate);
            _timeValidationServiceMock.Verify(x => x.ValidateTime(startTime, bedTime, _endTime), Times.Once);

        }
    }
}