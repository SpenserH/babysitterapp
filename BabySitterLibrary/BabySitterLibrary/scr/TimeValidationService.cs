﻿using System;

namespace BabySitterLibrary.scr
{
    public interface ITimeValidationService
    {
        bool ValidateTime(DateTime startTime, DateTime bedTime, DateTime endTime);
    }

    public class TimeValidationService
    {
        public bool ValidateTime(DateTime startTime, DateTime bedTime, DateTime endTime)
        {
            var midnight = new DateTime(endTime.Year, endTime.Month, endTime.Day, 0, 0, 0);
            return endTime >= startTime
                   && bedTime > startTime
                   && bedTime <= midnight
                   && ValidateStartTime(startTime)
                   && ValidateEndTime(startTime, endTime);
        }

        private static bool ValidateEndTime(DateTime startTime, DateTime endTime)
        {
            var validEndTime = new DateTime(endTime.Year, endTime.Month, startTime.Day+1, 4, 0, 0);
            return endTime <= validEndTime;
        }

        private static bool ValidateStartTime(DateTime startTime)
        {
            const int validStartHour = 17;
            return startTime.Hour >= validStartHour;
        }
    }
}