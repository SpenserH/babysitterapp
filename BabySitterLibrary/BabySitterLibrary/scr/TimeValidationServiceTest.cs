﻿using System;
using NUnit.Framework;

namespace BabySitterLibrary.scr
{
    [TestFixture]
    public class TimeValidationServiceTest
    {
        readonly TimeValidationService _timeValidationService = new TimeValidationService();
        private readonly DateTime _validStartTime = new DateTime(2015, 1, 1, 17, 0, 0);
        private readonly DateTime _validBedTime = new DateTime(2015,1,1,21,0,0);
        private readonly DateTime _validEndTime = new DateTime(2015, 1, 2, 4, 0, 0);

        [Test]
        public void ValidateStartAndEndTimeAreWithinValidRange()
        {
            Assert.IsTrue(_timeValidationService.ValidateTime(_validStartTime, _validBedTime, _validEndTime));
        }

        [Test]
        public void InValidWhenStartTimeisEarlierThan5Pm()
        {
            Assert.IsFalse(_timeValidationService.ValidateTime(new DateTime(2015, 1, 1, 16, 59, 59), _validBedTime, _validEndTime));
        }

        [Test]
        public void InValidWhenBedTimeIsLaterThanOrEqualToMidnight()
        {
            Assert.IsFalse(_timeValidationService.ValidateTime(_validStartTime, new DateTime(2015, 1, 2, 0, 0, 1), _validEndTime));
        }

        [Test]
        public void InValidWhenBedTimeIsEarlierThanStartTime()
        {
            Assert.IsFalse(_timeValidationService.ValidateTime(_validStartTime, new DateTime(2015, 1, 1, 16, 59, 59), _validEndTime));
        }

        [Test]
        public void InValidWhenEndtTimeIsEarlierThanStartTime()
        {
            Assert.IsFalse(_timeValidationService.ValidateTime(_validStartTime, _validBedTime, new DateTime(2015, 1, 1, 3, 0, 1)));
        }

        [Test]
        public void InValidWhenEndTimeIsLaterThan4AmOnSecondDay()
        {
            Assert.IsTrue(_timeValidationService.ValidateTime(_validStartTime, _validBedTime, new DateTime(2015, 1, 2, 3, 0, 1)));
            Assert.IsFalse(_timeValidationService.ValidateTime(_validStartTime, _validBedTime, new DateTime(2015, 1, 2, 4, 0, 1)));
        }

        [Test]
        public void InValidWhenEndTimeIsMoreThanOneDayLaterThanStartDate()
        {
            Assert.IsFalse(_timeValidationService.ValidateTime(_validStartTime, _validBedTime, new DateTime(2015, 1, 3, 2, 21, 1)));
        }
    }
}
